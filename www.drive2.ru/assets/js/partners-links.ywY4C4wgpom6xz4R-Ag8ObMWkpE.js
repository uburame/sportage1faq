var citilinkUrl = 'https://ad.admitad.com/g/bl69o59ui4fd7362cdd52b16fb31c7/?ulp=https%3A%2F%2Fwww.citilink.ru%2Fcatalog%2Fcar_electronics%2F';
var citilinkRoots = ['citilink.ru', 'citilink','ситилинк','ситилинка','ситилинку','ситилинке','ситилинком'];
d2.linkHighlighter(citilinkRoots, citilinkUrl, 'post', 'c-link c-link--citilink');
d2.linkHighlighter(citilinkRoots, citilinkUrl, 'comment', 'c-link c-link--citilink');

var carcamUrl = 'https://ad.admitad.com/g/jztq3fgl62fd7362cdd5ee6fec0cac/?ulp=https%3A%2F%2Fcarcam.ru';
var carcamRoots = ['carcam.ru','carcam','каркам','каркама','каркаму','каркамом','каркаме','КАР-КАМ','КАР КАМ'];
d2.linkHighlighter(carcamRoots, carcamUrl, 'post', 'c-link c-link--carcam');
d2.linkHighlighter(carcamRoots, carcamUrl, 'comment', 'c-link c-link--carcam');

var joomUrl = 'https://ad.admitad.com/g/m3cw8hjsqkfd7362cdd5adf070cfcc/?ulp=http%3A%2F%2Fjoom.com';
var joomRoots = ['joom.com', 'joom', 'Джум'];
d2.linkHighlighter(joomRoots, joomUrl, 'post', 'c-link c-link--joom');
d2.linkHighlighter(joomRoots, joomUrl, 'comment', 'c-link c-link--joom');

var goodsUrl = 'https://ad.admitad.com/g/hyddu57k03fd7362cdd5fbfd3e99ca/?ulp=https%3A%2F%2Fgoods.ru%2Fcatalog%2Favtotovary%2F';
var goodsRoots = ['goods.ru','goods'];
d2.linkHighlighter(goodsRoots, goodsUrl, 'post', 'c-link c-link--goods');
d2.linkHighlighter(goodsRoots, goodsUrl, 'comment', 'c-link c-link--goods');
